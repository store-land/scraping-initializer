from typing import Dict

from src.classes import AWS, Core
from src.utils.constants import LAMBDA_EXEC


def main(event: Dict, context) -> Dict:
    core = Core()
    stores_categories = core.get_stores_categories()
    if not stores_categories:
        return {"statusCode": 404, "message": "No stores/categories found :/"}

    stores = {}
    for row in stores_categories:
        store_id = row["storeId"]
        if store_id not in stores:
            stores[store_id] = {"store_slug": row["storeSlug"], "categories": []}

        stores[store_id]["categories"].append(
            {"id": row["categoryMapId"], "url": row["url"]}
        )

    aws = AWS()
    results = []
    for store_id, info in stores.items():
        store_slug = info["store_slug"]

        resp = aws.exec_lambda(
            LAMBDA_EXEC,
            payload={
                "storeId": store_id,
                "storeSlug": store_slug,
                "categories": info["categories"],
                "currentIdx": 0,
                "maxIdx": len(info["categories"]),
            },
        )
        results.append({"store": store_slug, "success": resp})

    return {"statusCode": 200, "results": results}
