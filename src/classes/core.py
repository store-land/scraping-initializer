import logging
import os
from functools import wraps
from typing import Dict, List, Optional, Union

import requests

from src.classes.http_client import HttpClient
from src.utils.constants import CORE_API_URL, LAMBDA_NAME

ENV = os.getenv("ENV")


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def func_log(f):
    @wraps(f)
    def decorate(*args, **kwargs):
        LOGGER.info(f"[{f.__name__}] Requesting...")
        data = f(*args, **kwargs)
        LOGGER.info(f"[{f.__name__}] Success: {data is not None}")
        return data

    return decorate


def format_response(
    resp: Optional[requests.models.Response],
) -> Optional[Union[Dict, List]]:
    if resp is None:
        return None

    if resp.status_code == 200:
        return resp.json()
    else:
        LOGGER.info(resp.text)
        return None


class Core:
    def __init__(self) -> None:
        self.API_URL = CORE_API_URL[ENV]
        self.http_client = HttpClient()
        self.http_client.session.headers["user-agent"] = f"{LAMBDA_NAME}-{ENV}"

    @func_log
    def get_stores_categories(self) -> List[Dict]:
        resp = self.http_client.get(f"{self.API_URL}/api/store/category")
        return format_response(resp)
