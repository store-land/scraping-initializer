CORE_API_URL = {
    "local": "http://localhost",
    "dev": "https://store-land-dev.herokuapp.com",
    "prod": "https://store-land.herokuapp.com",
}

LAMBDA_NAME = "scraping-initializer"
LAMBDA_EXEC = "scraping-service-prod-0CiYrN3FnRQt"
