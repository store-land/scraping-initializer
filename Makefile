install:
	pip3 install pipenv
	pipenv --three
	pipenv install

remove-unused:
	@echo "Remove unused..."
	autoflake --recursive --in-place --remove-all-unused-imports --remove-unused-variables ./

sort:
	@echo "Sorting..."
	isort src/

format:
	@echo "Blacking..."
	black src/

lint-fix: sort remove-unused format sort

requirements:
	@echo "Creating requirements.txt..."
	pipenv run pip freeze > requirements.txt

start:
	@echo "Run Local in Debug..."
	python index.py

pip-lock:
	@echo "Pipenv Lock..."
	pipenv lock

lambda-local:
	@echo "Run Lambda local..."
	sam local invoke prod
