from dotenv import load_dotenv
import logging
import sys

load_dotenv()

import os
import sentry_sdk

SENTRY_API = os.getenv("SENTRY_API")
if SENTRY_API:
    sentry_sdk.init(
        dsn=os.getenv("SENTRY_API"),
        traces_sample_rate=1.0,
    )

from src.app import main


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
LOGGER.addHandler(handler)


def handler(event, context):
    resp = main(event, context)
    LOGGER.info(resp)
    return resp
