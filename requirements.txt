python-dotenv==0.17.1
requests==2.25.1
sentry-sdk==1.1.0
boto3==1.17.73
